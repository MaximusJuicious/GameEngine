//
//  main.cpp
//
//  Created by Christopher Mahon 25/9/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#include "../include/main.h"

int main(int argc, char *argv[]) {
    try {
        Engine::init();
        Object obj("Fred");
        Object obj2("Not Fred");

        while (Engine::isRunning) {
            Engine::update();
        }

        Engine::destroy();
    }
    catch(std::exception& e) {
        std::cout << "EXCEPTION: BZZZZZT ERROR\n";
        std::cout << e.what() << '\n';
        return 1;
    }
    return 0;
}

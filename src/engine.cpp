//
//  engine.cpp
//  Engine class for GameEngine
//
//  Created by Christopher Mahon 28/11/18.
//  Copyright © 2018 Christopher. All rights reserved.
//

#include <math.h>

#include "../include/engine.hpp"

// Temporary variable for making the background fancy for testing
int i = 0;

bool Engine::isRunning = false;
Controls* Engine::control = new Controls();
std::vector<Object> Engine::entities(10);
SDL_Window* Engine::window = NULL;
SDL_Renderer* Engine::renderer = NULL;
SDL_Surface* Engine::screenSurface = NULL;


int Engine::init() {
    int ret = Engine::sdl_init();

    if (ret != 0) {
        std::cout << "Init failed!";
        return 1;
    }
    Engine::isRunning = true;
    return 0;
}

int Engine::destroy() {
    free(Engine::control);
    SDL_DestroyWindow(Engine::window);
    SDL_Quit();
    return 0;
}

int Engine::sdl_init() {
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
        printf("SDL INIT FAIL! %s\n", SDL_GetError());
        return 1;
    }
    // Create SDL window
    Engine::window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED, SCREEN_WIDTH, SCREEN_HEIGHT, 0);
    if (Engine::window == NULL) {
        printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
        return 2;
    }
    Engine::renderer = SDL_CreateRenderer(window, -1, 0);
    if (Engine::renderer == NULL) {
        printf("RENDERER could not be created! SDL_Error: %s\n",
            SDL_GetError());
        return 2;
    }
    // Get window surface
    Engine::screenSurface = SDL_GetWindowSurface(window);

    if (Engine::screenSurface == NULL) {
        printf("Surface could not be created! SDL_Error: %s\n", SDL_GetError());
        return 2;
    }
    return 0;
}

bool Engine::draw(int j) {
    // Fill the surface
    SDL_FillRect(Engine::screenSurface, NULL,
        SDL_MapRGB(Engine::screenSurface->format, j, j, j));

    for (auto& e : Engine::entities) e.draw();
    // Update the surface
    SDL_UpdateWindowSurface(window);

    return true;
}

bool Engine::update() {
    Uint32 frameStart;
    int frameTime;

    frameStart = SDL_GetTicks();

    i = (i+1) % 255;
    std::cout << "\nUPDATE!" << std::endl;
    control->update();
    for (auto& e : Engine::entities) e.update();
    draw(i);

    frameTime = SDL_GetTicks() - frameStart;

    if (frameDelay > frameTime) {
        SDL_Delay(frameDelay - frameTime);
    }
    Engine::isRunning = !control->quit;

    return true;
}

bool Engine::registerme(Object *obj) {
    // Push onto vector
    Engine::entities.push_back(*obj);
    return true;
}

bool Engine::unregister(Object *obj) {
    // Search and destroy

    return true;
}

//
//  List.cpp
//  triRefPointer
//
//  Created by Michael on 11/11/2017.
//  Copyright © 2017 Michael. All rights reserved.
//

#include "../include/list.hpp"

List::List():listHead(NULL), listTail(NULL) {
    // do nothing
}

List::~List() {
    Node *old;
    Node **tracer = &listHead;
    while (*tracer) {
        old = *tracer;
        tracer = &(*tracer)->next;
        delete old;
    }
}

void List::push(Object obj) {
    Node *temp = new Node(obj);

    if (temp == NULL) {
        std::cout << "Error overflow" << std::endl;
        exit(1);
    } else if (listHead == NULL) {
        listHead = temp;
        listTail = temp;
    } else {
        listTail->next = temp;
        listTail = temp;
    }
}

Object* List::search(std::string name) {
    Node **tracer = &listHead;
    Object *temp;
    bool found = false;

    while (*tracer && !(found = name.compare((*tracer)->data.objName))) {
        tracer = &(*tracer)->next;
    }

    if (found) {
        temp = &(*tracer)->data;
    } else {
        temp = NULL;
    }

    return temp;
}

void List::search_and_destroy(std::string name) {
    Node **tracer = &listHead;
    Node *old;
    bool found = false;

    while (*tracer && !(found = name.compare((*tracer)->data.objName))) {
        tracer = &(*tracer)->next;
    }

    if (found) {
        old = *tracer;
        *tracer = (*tracer)->next;
        delete old;
    }
}

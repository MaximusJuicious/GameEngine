//
//  object.cpp
//  object class for GameEngine
//  This class will be the object that components are attached to
//  This will allow things happen
//
//  Created by Christopher Mahon 25/9/17.
//  Copyright © 2017 Christopher. All rights reserved.
//


#include "../include/object.hpp"

Object::Object():objName("name") {
    coords = Vector2(0, 0);
    Engine::registerme(this);
}

Object::Object(std::string name):objName(name), active(true) {
    coords = Vector2(6, 9);
    Engine::registerme(this);
}

Object::Object(Vector2 location, std::string name):
    objName(name), coords(&location), active(true) {
    Engine::registerme(this);
}

Object::~Object() {
    // do nothing
}


void Object::update() {
    if (this->isActive()) {
        std::cout << "Update " << this->objName << std::endl;
        for (auto& c : this->components) c.update();
    }
}

void Object::draw() {
    if (this->isActive()) {
        std::cout << "Draw " << this->objName << std::endl;
        for (auto& c : this->components) c.draw();
    }
}

Vector2 Object::position() {
    return coords;
}

void Object::move(Vector2 move_dir) {
    this->coords+=move_dir;
}

# OBJS specifies which files to compile as part of the project
SOURCE_DIR = src
BINARY_DIR = bin
DOC_DIR = docs
BUILD_DIR = _build

# List of source files
sources = $(wildcard $(SOURCE_DIR)/*.cpp)
doc_sources = $(wildcard $(DOC_DIR)/*.md)

doc_output:= $(patsubst $(DOC_DIR)/%.md, $(BUILD_DIR)/%.html, $(doc_sources))
# build_output specifies the name of our exectuable
build_output = GameEngine

CC = g++
CPP_STANDARD = -std=c++17

# Ubuntu properly links everything when installed through apt, but Windows needs special treatment
ifeq ($(OS), Windows_NT)
	ifndef SDL_LOCATION 
		SDL_LOCATION = C:\external_libs\SDL2
	endif
	PLATFORM_INCLUDE = -I$(SDL_LOCATION)\include
	PLATFORM_LIBRARY = -L$(SDL_LOCATION)\lib
	PLATFORM_COMPILER_FLAGS = -Wl,-subsystem,console -Dmain=SDL_main
	PLATFORM_LINKER_FLAGS = -lmingw32 -lSDL2main -mwindows
# Commenting for the time being until we prepare a version of SDL2 for mingw23
# else ifeq ($(target), Windows)
# 	ifndef SDL_LOCATION 
# 		SDL_LOCATION = /usr
# 	endif
# 	CC = x86_64-w64-mingw32-g++-win32
# 	PLATFORM_INCLUDE = -I$(SDL_LOCATION)\include
# 	PLATFORM_LIBRARY = -L$(SDL_LOCATION)\lib
# 	PLATFORM_COMPILER_FLAGS = -Wl,-subsystem,console -Dmain=SDL_main
# 	PLATFORM_LINKER_FLAGS = -lmingw32 -lSDL2main -mwindows
else 
	PLATFORM_INCLUDE =
	PLATFORM_LIBRARY =
	PLATFORM_COMPILER_FLAGS =
	PLATFORM_LINKER_FLAGS =
endif

COMPILER_FLAGS = $(CPP_STANDARD) $(PLATFORM_COMPILER_FLAGS) -w

LINKER_FLAGS = $(PLATFORM_LINKER_FLAGS) -lm -lSDL2 

INCLUDE_PATHS = $(PLATFORM_INCLUDE)
LIBRARY_PATHS = $(PLATFORM_LIBRARY)

# This is the target that compiles our exeCutable
all: clean
	$(CC) $(sources) $(LIBRARY_PATHS) $(INCLUDE_PATHS) $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(BINARY_DIR)/$(build_output)

man: $(BUILD_DIR) $(doc_output)

$(BUILD_DIR):
	mkdir $(BUILD_DIR)

# http://www.gnu.org/software/make/manual/make.html#Static-Pattern
$(doc_output):$(BUILD_DIR)/%.html: $(DOC_DIR)/%.md
	pandoc -s $< -o $@

clean:
	rm -rf bin/*.out
	rm -rf src/*.o
	rm -rf _build

Game Engine Project
===================

<strong>Now with 100% more deprecation<br>
This project has been replaced [here](https://gitlab.com/sdl-game-dev/game-engine.git) </strong>

Description
===========
Game Engine created using C++ and SDL2 
Component based system  


<br/>
<strong>What the engine will incorporate:</strong>

* Basic object/entity that can be enhanced (past the basic move/collide features, components built into the engine will be subject to experimentation)

* Control support (Initially just keyboard, look into controller with SDL and others)  

* Collision detection  

* Graphical and Audio APIs


<strong>Standards for coding</strong>

* Style: Google C++ 

* Filenames: cpp and hpp (main does not belong so its an exception), all lower case 

* Classes: Camel case (HelloThere)

* Functions: all lowercase with underscores (hello_there)

* Variables: Camel case with a lowercase first leter (helloThere)


<strong>Controls</strong>

* Esc to quit (the window's x does the job just as well)

* Up, Down, Left, and Right arrows will set the corresponding flags in Engine's controller (Although they do nothing yet)

* Other Buttons to be implemented


Technologies
============
C++ (17), libSDL-dev  

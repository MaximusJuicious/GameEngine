Installing development packages
======================

**Ubuntu**  
- apt install git gcc make libsdl2-2.0-0 pandoc  
- pip3 install cpplint  

**Windows (MinGW)**  
- install git (https://git-scm.com/downloads)  
- install mingw32 (https://sourceforge.net/projects/mingw/files/)  
- install gcc and mingw32-make through mingw  
- download SDL2 for MinGW (https://www.libsdl.org/release/SDL2-devel-2.0.8-mingw.tar.gz)  
- install SDL2 packages at `C:\external_libs\SDL2`. This is where the Makefile expects them (this can be overriden for users by passing in `SDL_LOCATION=<PATH TO SDL>).  
- Install cpplint through pip (Requires Python 3)


Building binary
===============

**Ubuntu / Mac**  
- make all  

**Ubuntu / Mac build for windows**  
- should just require installing mingw and passing TARGET=Windows. Needs future research

**Windows (MinGW32)**  
- mingw32-make all  


Building documentation
======================

**Ubuntu / Mac**  
- make man  

**Ubuntu / Mac build for windows**  
- should just require installing mingw and passing TARGET=Windows. Needs future research

**Windows (MinGW32)**  
- mingw32-make man  

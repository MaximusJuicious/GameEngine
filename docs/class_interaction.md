Classes
=======

**List of classes**

* Engine
* Object
* Component
* Controller

**Engine**

The main logic loop for the engine. Primarily responsible for managing global SDL variables, managing controller inputs and managing game objects 

This class does not need to be instanciated, we use class variables and methods so we dont need to pass references back and forward through classes.

This class has a "Controller" instance to read keyboard/controller input.

This class has a list of "Object" class instances. The engine update and draw methods will loop through this list to execute these methods.

**Object**

The main object in the game. This class is an object that amongst other functionality acts as a container of "Components". When the Engine calls Update and Draw on an Object, the Object will in turn call the Update and Draw of each of its Components. 

**Component**

A Component defines the functionality for an "Object". A base class is provided for components and the user is expected inherit from this, implement their own functionality and then attach the component to an "Object".

**Controller**

Reads end user input through keyboard and controller(eventually) interfaces. Then sets variables depending on the input.

References
==========
For game engine list  
* Thread: https://gamedev.stackexchange.com/questions/33888/what-is-the-most-efficient-container-to-store-dynamic-game-objects-in

Tutorials
=========
For SDL
* Text: http://lazyfoo.net/tutorials/SDL/index.php 
* Video: https://www.youtube.com/playlist?list=PLhfAbcv9cehhkG7ZQK0nfIGJC_C-wSLrx

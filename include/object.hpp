//
//  object.hpp
//  object class for GameEngine
//  This class will be template for game developers to overload
//
//  Created by Christopher Mahon 25/9/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once

#include <stdio.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <string>

#include "prototypes.hpp"
#include "vector2.hpp"
#include "engine.hpp"
#include "component.hpp"

class Object {
friend class Engine;
 public:
    Object();
    explicit Object(std::string name);
    Object(Vector2 location, std::string name);
    ~Object();

    std::string objName;

    void update();
    void draw();

    Vector2 position(void);
    void move(Vector2);
    bool isActive() const { return active; }
    void destroy() { active = false; }

    bool addComponent();
    void deleteComponent();

 protected:
    std::string spriteName;
    Vector2 coords;

 private:
    bool active = false;
    std::vector<Component> components;
};

//
//  controls.hpp
//  Controls class for GameEngine
//  This class will be responsible for interfacing with keyboardfor key input
//
//  Created by Christopher Mahon 7/11/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once
#include <SDL2/SDL.h>
#include <stdio.h>

class Controls {
 public:
    Controls();
    ~Controls();

    bool a, b, x, y, start;
    bool left, right, up, down;
    bool quit;
    int update();

 protected:
    int reset();
};

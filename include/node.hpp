//
//  Node.hpp
//  triRefPointer
//
//  Created by Michael on 11/11/2017.
//  Copyright © 2017 Michael. All rights reserved.
//

#pragma once

#include <stdio.h>
#include "object.hpp"

class Node{
friend class List;
 public:
    // constructor
    explicit Node(Object);
    // destructor
    ~Node();

 private:
    Node *next;
    Node **tracer;
    Object data;
};

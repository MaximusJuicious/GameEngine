//
//  prototypes.hpp
//  Used for forward declaration of classes
//
//  Created by Christopher Mahon 13/11/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once

class Vector2;
class Controls;
class Node;
class Object;
class Engine;

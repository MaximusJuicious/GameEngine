//
//  List.hpp
//  triRefPointer
//
//  Created by Michael on 11/11/2017.
//  Copyright © 2017 Michael. All rights reserved.
//

#pragma once

#include <stdio.h>
#include <iostream>
#include <string>

#include "node.hpp"
#include "object.hpp"


class List{
 public:
    // constructor
    List();
    ~List();
    void push(Object obj);
    Object* search(std::string name);
    void search_and_destroy(std::string name);
    void pop(Object* obj);

 private:
    Node *listHead, *listTail;
};

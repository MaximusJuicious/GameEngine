//
//  vector2.hpp
//  Vector2 class for GameEngine
//  This class will be for holding all xy co-ordinates
//  May eventually get some geometry functions in here
//
//  Created by Christopher Mahon 28/6/2016.
//  Copyright © 2016 Christopher Mahon. All rights reserved.
//

#pragma once

class Vector2 {
 public:
    double x, y;

    Vector2(double tX, double tY);
    Vector2(int tX, int tY);
    explicit Vector2(Vector2 *vect);
    Vector2();
    ~Vector2();

    double Distance(Vector2);
    double DiffX(Vector2);
    double DiffY(Vector2);

    Vector2 operator+(Vector2);
    Vector2 operator+(int);
    Vector2 operator+(float);

    void operator+=(Vector2);
    void operator+=(int);
    void operator+=(float);

    Vector2 operator-(Vector2);
    Vector2 operator-(int);
    Vector2 operator-(float);

    Vector2 operator*(Vector2);
    Vector2 operator*(int);
    Vector2 operator*(float);

    Vector2 operator/(Vector2);
    Vector2 operator/(int);
    Vector2 operator/(float);

    Vector2 SetValues(double x, double y);
    Vector2 SetValues(Vector2);
};

// 
//  main.h
//  Temporary File
//  Intended for testing the engine
//
//  Created by Christopher Mahon 13/11/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once
#include <iostream>
#include <exception>

#include "engine.hpp"

int main(int, char**);

//
//  component.hpp
//  component class that will be used as a basis for all
//      other components
//  Used https://www.youtube.com/watch?v=XsvI8Sng6dk as
//      initial starting point
//
//  Created by Christopher Mahon 7/11/17.
//  Copyright © 2017 Christopher. All rights reserved.
//

#pragma once
#include <iostream>

#include "object.hpp"

class Component {
 public:
    Object* entity;

    virtual void init() {}
    virtual void update() {}
    virtual void draw() {}
    virtual ~Component() {}
};

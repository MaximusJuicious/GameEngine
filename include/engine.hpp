//
//  engine.hpp
//  Engine class for GameEngine
//
//  Created by Christopher Mahon 28/11/18.
//  Copyright © 2018 Christopher. All rights reserved.
//

#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>

#include "SDL2/SDL.h"

#include "prototypes.hpp"
#include "controls.hpp"
#include "object.hpp"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
const int FPS = 60;
const float frameDelay = 1000 / FPS;

class Engine {
 public:
    Engine();
    ~Engine();
    static bool registerme(Object *obj);
    static bool unregister(Object *obj);
    static int init();
    static int sdl_init();
    static bool update();
    static int destroy();

    Controls* controller() const {return Engine::control;}

    static Controls* control;
    static bool isRunning;
    static std::vector<Object> entities;

 protected:
    static bool draw(int);
    static SDL_Window *window;
    static SDL_Renderer *renderer;
    static SDL_Surface *screenSurface;
};
